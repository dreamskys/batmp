<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"  %>
<%@ include file="/WEB-INF/public/taglib.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>用户登录</title>
<%@ include file="/WEB-INF/public/css.jsp" %>
<link rel="stylesheet" href="${staticPath}/css/login.css" />
</head>
<body>
<div id="loginbox">            
<!-- http://themedesigner.in/demo/matrix-admin/index.html -->
<form id="loginform" class="form-vertical" action="${contextPath}/user/login" method="post" target="login-iframe">
<iframe id="login-iframe" name="login-iframe" style="display: none;"></iframe>
<div class="control-group normal_text"> <h3><img src="${staticPath}/img/logo.png" alt="Logo" /></h3></div>
<div class="control-group">
    <div class="controls">
        <div class="main_input_box">
            <span class="add-on bg_lg"><i class="icon-user"></i></span><input type="text" placeholder="用户名：" name="userName" autocomplete="off" autofocus="autofocus" />
        </div>
    </div>
</div>
<div class="control-group">
    <div class="controls">
        <div class="main_input_box">
            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" placeholder="密码：" name="password" />
        </div>
    </div>
</div>
<div class="form-actions">
    <span><a href="javascript:" id="to-recover">[忘记密码]</a></span>&nbsp;&nbsp;
    <span><a href="javascript:">[新用户注册]</a></span>
    <span class="pull-right"><a type="submit" class="btn btn-success" id="login-btn" > 登 录 <i class="icon-forward"></i></a></span>
</div>
  <div class="alert alert-info" id="login-error" style="display: none;text-align: center;">
	</div>
</form>
<form id="recoverform" action="#" class="form-vertical">
<div class="control-group">
<p class="normal_text">下面输入您的电子邮件地址,我们将给你发送指令如何恢复密码</p>
<div class="controls">
    <div class="main_input_box input-control">
            <span class="add-on bg_lo" style="text-align: center;"><i class="icon-envelope"></i></span><input type="text"  placeholder="注册的邮箱地址" />
    </div>
    <div class="main_input_box input-control">
            <span class="add-on bg_lo" style="text-align: center;"><i class="icon-barcode"></i></span><input type="text"  placeholder="验证码" style="width:80px;"/>
    </div>
    <div class="main_input_box input-control">
    	  <span><img src="https://mp.weixin.qq.com/cgi-bin/verifycode?username=2492978561@qq.com&r=1390189389456" style="height: 38px;"/></span>
    	  <span><a href="#">看不清,换一张</a></span>
    </div>
</div>
</div>
     <div class="form-actions" style="margin: 0px;">
         <span class="pull-left"><a class="flip-link btn btn-success" id="to-login"><i class="icon-backward"></i> 返 回</a></span>
         <span class="pull-right"><a class="btn btn-success"> 确 认 <i class="icon-forward"></i></a></span>
     </div>
</form>
</div>
<script src="${staticPath}/js/jquery.min.js"></script>
<script src="${staticPath}/js/bootstrap.min.js"></script>  
<script src="${staticPath}/js/ui.login.js"></script> 
<script src="${staticPath}/js/modal-message.js"></script>
<script src="${staticPath}/js/user/login.js"></script> 
<script type="text/javascript">
$(function(){
	
});
</script>
</body>
</html>
