<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"  %>
<%@ include file="/WEB-INF/public/taglib.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>后台管理</title>
<%@ include file="/WEB-INF/public/css.jsp" %>
<link rel="stylesheet" href="${staticPath}/css/matrix-style.css" />
<link rel="stylesheet" href="${staticPath}/css/matrix-media.css" />
<link rel="stylesheet" href="${staticPath}/font-awesome/css/font-awesome.css"/>
<link rel="stylesheet" href="${staticPath}/css/jquery.gritter.css" />
</head>
<body>
<!--Header-part-->
<div id="header">
  <h1><a href="dashboard.html">Matrix Admin</a></h1>
</div>
<!--close-Header-part--> 
<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" >
      <a href="javascript:" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i><span class="text"> 欢迎 管理员 </span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="javascript:"><i class="icon-user"></i>&nbsp;账号信息 </a></li>
        <li class="divider"></li>
        <li><a href="javascript:"><i class="icon-check"></i>&nbsp;修改密码 </a></li>
        <li class="divider"></li>
        <li><a href="javascript:" onclick="$.modal.confirm('确认退出?',function(r){if(r){location.href='${contextPath}/logout';}});"><i class="icon-key"></i>&nbsp;退出登录 </a></li>
      </ul>
    </li>
    <li class="dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text"> 消 息 </span> <span class="label label-important">5</span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a class="sAdd" title="" href="#"><i class="icon-plus"></i> new message</a></li>
        <li class="divider"></li>
        <li><a class="sInbox" title="" href="#"><i class="icon-envelope"></i> inbox</a></li>
        <li class="divider"></li>
        <li><a class="sOutbox" title="" href="#"><i class="icon-arrow-up"></i> outbox</a></li>
        <li class="divider"></li>
        <li><a class="sTrash" title="" href="#"><i class="icon-trash"></i> trash</a></li>
      </ul>
    </li>
    <li class=""><a title="" href="#"><i class="icon icon-cog"></i> <span class="text"> 设 置 </span></a></li>
    <li class=""><a title="" href="${contextPath}/logout"><i class="icon icon-share-alt"></i> <span class="text"> 退 出</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->
<!--start-top-serch
<div id="search">
  <input type="text" placeholder="Search here..."/>
  <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
</div>
-->
<!--close-top-serch-->
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="active"><a href="${contextPath}"><i class="icon icon-home"></i> <span> 主界面 </span></a> </li>
    <li><a href="charts.html"><i class="icon icon-user"></i> <span> 账号管理 </span></a> </li>
    <li class="submenu"><a href="javascript:"><i class="icon icon-envelope"></i> <span> 消息设置 </span><span class="label label-important">3</span></a>
    	<ul>
	        <li><a href=""> 被关注回复 </a></li>
	        <li><a href=""> 关键字回复 </a></li>
	        <li><a href=""> 默认回复 </a></li>
       </ul>
    </li>
    <li><a href="charts.html"><i class="icon icon-signal"></i> <span> 自定义菜单 </span></a> </li>
    <li class="submenu"><a href="javascript:"><i class="icon icon-inbox"></i> <span> 活动管理 </span><span class="label label-important">4</span></a>
    	<ul>
	        <li><a href=""> 刮刮卡 </a></li>
	        <li><a href=""> 大转盘 </a></li>
	        <li><a href=""> 砸金蛋 </a></li>
	        <li><a href=""> 节日贺卡 </a></li>
       </ul>
    </li>
    <li><a href="${contextPath}/shop"><i class="icon icon-th"></i> <span> 商城管理 </span></a>
    </li>
  </ul>
</div>
<!--sidebar-menu-->

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="${contextPath}" title=" 返回首页 " class="tip-bottom"><i class="icon-home"></i> 首 页 </a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
 <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lb span3"><a href="index.html"> <i class="icon-dashboard"></i> <span class="label label-important">20</span> My Dashboard </a> </li>
        <li class="bg_lg span3"><a href="charts.html"> <i class="icon-signal"></i> Charts</a> </li>
        <li class="bg_ly span3"><a href="widgets.html"> <i class="icon-inbox"></i><span class="label label-success">101</span> Widgets </a> </li>
        <li class="bg_lo span3"><a href="tables.html"> <i class="icon-th"></i> Tables</a> </li>
        <li class="bg_ls span3"><a href="grid.html"> <i class="icon-fullscreen"></i> Full width</a> </li>
        <li class="bg_lo span3"><a href="form-common.html"> <i class="icon-th-list"></i> Forms</a> </li>
        <li class="bg_ls span3"><a href="buttons.html"> <i class="icon-tint"></i> Buttons</a> </li>
        <li class="bg_lb span3"><a href="interface.html"> <i class="icon-pencil"></i>Elements</a> </li>
        <li class="bg_lg span3"><a href="calendar.html"> <i class="icon-calendar"></i> Calendar</a> </li>
        <li class="bg_lr span3"><a href="error404.html"> <i class="icon-info-sign"></i> Error</a> </li>
      </ul>
    </div>
  </div>
</div>
<!--Footer-part-->
<div class="row-fluid">
  <div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in/">Themedesigner.in</a> </div>
</div>
<!--end-Footer-part-->
<script src="${staticPath}/js/excanvas.min.js"></script> 
<script src="${staticPath}/js/jquery.min.js"></script> 
<script src="${staticPath}/js/jquery.ui.custom.js"></script> 
<script src="${staticPath}/js/bootstrap.min.js"></script> 
<script src="${staticPath}/js/jquery.flot.min.js"></script> 
<script src="${staticPath}/js/jquery.flot.resize.min.js"></script> 
<script src="${staticPath}/js/jquery.peity.min.js"></script> 
<script src="${staticPath}/js/fullcalendar.min.js"></script> 
<script src="${staticPath}/js/matrix.js"></script> 
<script src="${staticPath}/js/matrix.dashboard.js"></script> 
<script src="${staticPath}/js/jquery.gritter.min.js"></script> 
<script src="${staticPath}/js/matrix.interface.js"></script> 
<script src="${staticPath}/js/matrix.chat.js"></script> 
<script src="${staticPath}/js/jquery.validate.js"></script> 
<script src="${staticPath}/js/matrix.form_validation.js"></script> 
<script src="${staticPath}/js/jquery.wizard.js"></script> 
<script src="${staticPath}/js/jquery.uniform.js"></script> 
<script src="${staticPath}/js/select2.min.js"></script> 
<script src="${staticPath}/js/matrix.popover.js"></script> 
<script src="${staticPath}/js/jquery.dataTables.min.js"></script> 
<script src="${staticPath}/js/matrix.tables.js"></script>
<script src="${staticPath}/js/modal-message.js"></script> 
<!-- 
<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
 -->
</body>
</html>
