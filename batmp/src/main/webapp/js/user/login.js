(function(){
	$(document).ready(function(){
		$("#login-btn").click(function(){
			var userName = $("input[name='userName']");
			var password = $("input[name='password']");
			if(userName.val().length <= 0){
				userName.css("background-color","#ffebec").focus();
				$("#login-error").html("用户名不能为空!").show();
				return;
			}else{
				userName.css("background-color","#ffffff");
				$("#login-error").slideUp(50);
			}
			if(password.val().length <= 0){
				password.css("background-color","#ffebec").focus();
				$("#login-error").html("密码不能为空!").slideDown();
				return;
			}else{
				password.css("background-color","#ffffff");
				$("#login-error").slideUp(50);
			}
			$(this).addClass("disabled").html(' 登录中 <i class="icon-forward"></i>');
			$("#loginform").submit();
			
		});
	});
	$("#login-iframe").load(function(){
		var msg = $.parseJSON($(this).contents().find("body").text());
		if(msg.status == "ok"){
			location.href = "/bat/";
		}else{
			$("#login-error").slideDown(50).html(msg.msg);
		}
	});
})();