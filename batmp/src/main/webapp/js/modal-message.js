(function(){
var cache = {
	modalDiv:function(){return $('<div class="modal hide fade" style="width:280px;border-radius:3px;margin:0 auto;"/>');},
	headerDiv:function(){return $('<div class="modal-header" style="height:20px;padding:9px 10px;"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><span><i class="icon icon-comments-alt"></i> 提 示</span></div>');},
	bodyDiv:function(text){return $('<div class="modal-body"><p>&nbsp;&nbsp;'+text+'</p></div>');},
	footerDiv:function(){return $('<div class="modal-footer" style="height:30px;padding:5px 10px 5px 5px;"/>');}
};
$.extend({
	modal:{
		alert:function(text,fn){
		    var modal = cache.modalDiv().append(cache.headerDiv()).append(cache.bodyDiv(text));
		    var btn = $("<a class='btn btn-success' type='submit'> 确 认 </a>");
		    btn.click(function(){modal.modal('hide');});
		    modal.append(cache.footerDiv().append(btn));
		    $("body").append(modal);
			var m_w = modal.width()/2;var m_h  = modal.height();
		    var w_w = $(window).width()/2;var w_h = $(window).height()/2;
		    modal.css("left",w_w-m_w+"px").css("top",w_h-m_h+"px");
		    modal.modal('show').on('hidden', function (){modal.remove();if(typeof fn == "function"){fn();}});
		},
		confirm:function(text,fn){
			  var modal = cache.modalDiv().append(cache.headerDiv()).append(cache.bodyDiv(text));
			  var r = false;
			  var btn1 = $("<a class='btn btn-success' type='submit'> 确 认 </a>").focus();
			  var btn2 = $("<a class='btn btn-success' type='submit' style='margin-left:10px;'> 取 消 </a>");
			  btn1.click(function(){r=true;modal.modal('hide');});
			  btn2.click(function(){r=false;modal.modal('hide');});
			  modal.append(cache.footerDiv().append(btn1).append(btn2));
		      $("body").append(modal);
			  var m_w = modal.width()/2;var m_h  = modal.height();
		      var w_w = $(window).width()/2;var w_h = $(window).height()/2;
		      modal.css("left",w_w-m_w+"px").css("top",w_h-m_h+"px");
		      modal.modal('show').on('hidden', function (){modal.remove();if(typeof fn == "function"){fn(r);}});
		},
		prompt:function(){
			var input_p = "<p><input type=''/><p>";
			var modal = cache.modalDiv().append(cache.headerDiv()).append(cache.bodyDiv(text));
			var btn1 = $("<a class='btn btn-success' type='submit'> 确 认 </a>").focus();
			var btn2 = $("<a class='btn btn-success' type='submit' style='margin-left:10px;'> 取 消 </a>");
		}
	}
});
})();	