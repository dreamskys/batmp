package bat.config;

import bat.web.controller.ApiController;
import bat.web.controller.IndexController;
import bat.web.controller.UserController;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.render.ViewType;

public class BatConfig extends JFinalConfig
{

	@Override
	public void configConstant(Constants constants)
	{
		constants.setBaseViewPath("/WEB-INF/jsp");
		constants.setDevMode(true);
		constants.setViewType(ViewType.JSP);
	}

	@Override
	public void configHandler(Handlers handlers)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void configInterceptor(Interceptors interceptors)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void configPlugin(Plugins plugins)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void configRoute(Routes routes)
	{
		routes.add("/",IndexController.class);
		routes.add("/api", ApiController.class);
		routes.add("/user", UserController.class);
		routes.add("/account", bat.web.controller.account.IndexController.class);
		
		//admin
		routes.add("/admin",bat.web.controller.admin.IndexController.class);
		routes.add("/admin/users",bat.web.controller.admin.UserController.class);
	}

}
