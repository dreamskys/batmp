package bat.web.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import com.jfinal.core.Controller;


public class UserController extends Controller
{
	/**
	 * 用户登录
	 */
	public void login()
	{
		Map<String,Object> loginMsg = new LinkedHashMap<String,Object>();
		loginMsg.put("status","ok");
		loginMsg.put("msg", "登录成功!");
		setSessionAttr("user", loginMsg);
		renderJson(loginMsg);
	}
}
