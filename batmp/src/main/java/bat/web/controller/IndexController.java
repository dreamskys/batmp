package bat.web.controller;

import com.jfinal.core.Controller;

public class IndexController extends Controller
{
	/**
	 *首页 
	 */
	public void index()
	{
		render("index.jsp");
	}
	
	/**
	 * 登录界面
	 */
	public void login()
	{
		render("user/login.jsp");
	}
	
	/**
	 * 退出
	 */
	public void logout()
	{
		redirect("/login");
	}
}
